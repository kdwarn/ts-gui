# ts-gui

A GUI implementation of [timestudy](https://codeberg.org/kdwarn/timestudy), an activity tracker library, created with [Iced](https://iced.rs).

## Project status

It is currently a rough, early work in progress.

Features, enhancements, bugs, questions, and similar are tracked in [issues](https://codeberg.org/kdwarn/ts-gui/issues). These are then distilled into [milestones](https://codeberg.org/kdwarn/ts-gui/milestones). I intend to regularly update the next two milestones as a way to think about and plan the next immediate things to do.

## Local development

`cargo watch -x run` to keep updated app running as it's being worked on.

Use the `debug` feature to be able to press F12 and see some debug info like number of messages called.
