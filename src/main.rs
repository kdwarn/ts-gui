use std::collections::VecDeque;

use chrono::Local;
use once_cell::sync::Lazy;

use iced::alignment::{Horizontal, Vertical};
use iced::widget::{button, column, container, row, scrollable, text, text_input, Column, Space};
use iced::{executor, Application, Command, Length, Settings};
use timestudy::{past_activities, Activity};

// bring in the custom theme
mod ts_theme;
use ts_theme::TimeStudyTheme;

static TAG_INPUT: Lazy<text_input::Id> = Lazy::new(text_input::Id::unique);

/// (Re)declare these types to work with the custom theme.
pub type Renderer = iced::Renderer<TimeStudyTheme>;
pub type Element<'a, Message> = iced::Element<'a, Message, Renderer>;

pub fn main() -> iced::Result {
    App::run(Settings::default())
}

#[derive(Debug, Clone)]
enum Msg {
    Start,
    Stop,
    TagsInputChanged(String),
    EditActivity(Activity),
    DeleteActivity(usize),
}

struct App {
    current_activity: Option<Activity>,
    past_activities: VecDeque<Activity>,
    tags: Option<Vec<String>>,
    msg_to_user: Option<String>,
}

impl App {
    /// Get current activity and past activities.
    fn current_and_past(&mut self) {
        match Activity::current() {
            Ok(Some(v)) => self.current_activity = Some(v),
            Ok(None) => self.current_activity = None,
            Err(e) => {
                self.current_activity = None;
                self.msg_to_user = Some(e.to_string());
            }
        }
        match past_activities() {
            Ok(Some(v)) => self.past_activities = v,
            Ok(None) => self.past_activities = VecDeque::new(),
            Err(e) => {
                self.past_activities = VecDeque::new();
                self.msg_to_user = Some(e.to_string());
            }
        }
    }
}

impl Application for App {
    type Executor = executor::Default;
    type Flags = ();
    type Message = Msg;
    type Theme = TimeStudyTheme;

    fn title(&self) -> String {
        String::from("timestudy")
    }

    fn new(_flags: ()) -> (Self, Command<Self::Message>) {
        let mut app = Self {
            current_activity: None,
            past_activities: VecDeque::new(),
            tags: None,
            msg_to_user: None,
        };
        app.current_and_past();
        // return app, put focus on tag text_input
        (app, text_input::focus::<Msg>(TAG_INPUT.clone()))
    }

    fn view(&self) -> Element<Msg> {
        let mut main_content = column![];
        let dt_fmt = "%a %b %e, %Y at %l:%M%p";

        let current_status = match &self.current_activity {
            None => "No current activity".to_string(),
            Some(v) => format!(
                "Current activity started on\n {}",
                v.start.with_timezone(&Local).format(dt_fmt)
            ),
        };
        main_content = main_content.push(
            text(current_status)
                .horizontal_alignment(Horizontal::Center)
                .size(40)
                .width(Length::Fill),
        );

        if let Some(v) = &self.current_activity {
            // show any existing tags on the activity
            if let Some(tags) = &v.tags {
                main_content = main_content.push(
                    text(&format!("Tagged: {}", &tags.join(", ")))
                        .width(Length::Fill)
                        .horizontal_alignment(Horizontal::Center),
                );
            }
        } else {
            // Create text input for user to add tags to a new activity.
            // For now, accept a space-separated list of tags.
            // In the future, enter one at a time and allow for completions
            let tags = match &self.tags {
                Some(v) => v.clone().join(" "),
                None => "".to_string(),
            };

            main_content = main_content.push(
                text_input(
                    "Tags (separated by a space)...",
                    &tags,
                    Msg::TagsInputChanged,
                )
                .size(30)
                .on_submit(Msg::Start)
                .id(TAG_INPUT.clone()),
            );
        }

        let button_text: &str;
        let button_msg: Msg;

        match &self.current_activity {
            None => {
                button_text = "Start";
                button_msg = Msg::Start;
            }
            Some(_) => {
                button_text = "Stop";
                button_msg = Msg::Stop;
            }
        }

        main_content = main_content.push(row![
            Space::with_width(Length::Fill),
            button(
                text(button_text)
                    .size(50)
                    .horizontal_alignment(Horizontal::Center)
                    .vertical_alignment(Vertical::Center),
            )
            .width(Length::Units(200))
            .height(Length::Units(200))
            .on_press(button_msg)
            .style(ts_theme::Button::StartStop),
            Space::with_width(Length::Fill)
        ]);

        let msg_to_user = match &self.msg_to_user {
            None => "".to_string(),
            Some(v) => v.to_string(),
        };
        main_content = main_content.push(
            text(msg_to_user)
                .horizontal_alignment(Horizontal::Center)
                .width(Length::Fill)
                .size(30),
        );

        // build list of past activities and controls within it
        let past_activities = self.past_activities.iter().fold(
            Column::with_children(vec![container(
                text("Past Activities")
                    .size(30)
                    .width(Length::Fill)
                    .horizontal_alignment(Horizontal::Center),
            )
            .width(Length::Fill)
            .into()])
            .spacing(20)
            .padding([20, 20]),
            |col, activity| {
                // // If there's a current activity, we need to adjust the index
                // // because `timestudy` acts on all activities, not just past.
                // if Activity::current().unwrap().is_some() {
                //     index += 1
                // }
                let tags = if let Some(v) = &activity.tags {
                    v.join(", ")
                } else {
                    "No tags".to_string()
                };

                // Create easy-to-read duration
                let mut friendly_duration = vec![];
                let duration = activity.end.unwrap() - activity.start;
                match duration.num_days() {
                    0 => (),
                    1 => friendly_duration.push("1 day".to_string()),
                    _ => friendly_duration.push(format!("{} days", &duration.num_days())),
                };
                match duration.num_hours() {
                    0 => (),
                    1 => friendly_duration.push("1 hour".to_string()),
                    _ => friendly_duration.push(format!("{} hours", &duration.num_hours() % 60)),
                };
                match duration.num_minutes() {
                    0 => (),
                    1 => friendly_duration.push("1 minute".to_string()),
                    _ => {
                        friendly_duration.push(format!("{} minutes", &duration.num_minutes() % 60))
                    }
                };
                if duration.num_minutes() < 1 {
                    friendly_duration.push("less than 1 minute".to_string());
                }

                col.push(
                    container(column![
                        row![text(tags)
                            .width(Length::Fill)
                            .horizontal_alignment(Horizontal::Right)],
                        row![text(format!(
                            "{} - {}",
                            activity.start.with_timezone(&Local).format(dt_fmt),
                            activity.end.unwrap().with_timezone(&Local).format(dt_fmt)
                        ))
                        .width(Length::Fill)
                        .horizontal_alignment(Horizontal::Right)],
                        row![text(format!("({})", friendly_duration.join(", ")))
                            .width(Length::Fill)
                            .horizontal_alignment(Horizontal::Right)],
                        row![
                            Space::with_width(Length::Fill),
                            button("Edit")
                                .on_press(Msg::EditActivity(activity.clone()))
                                .style(ts_theme::Button::PastActivity),
                            button("Delete")
                                .on_press(Msg::DeleteActivity(activity.get_index().unwrap()))
                                .style(ts_theme::Button::PastActivity),
                        ]
                        .width(Length::Fill)
                        .spacing(10)
                        .padding([10, 0, 0, 0])
                    ])
                    .padding(10)
                    .style(ts_theme::Container::PastActivity),
                )
            },
        );

        // put it all together
        row![
            main_content
                .spacing(20)
                .padding(15)
                .width(Length::FillPortion(1)),
            column![scrollable(past_activities)].width(Length::FillPortion(1))
        ]
        .into()
    }

    fn update(&mut self, message: Msg) -> Command<Self::Message> {
        match message {
            Msg::Start => match Activity::start(None, self.tags.clone()) {
                Ok(_) => {
                    self.msg_to_user = Some("Activity Started".to_string());
                    self.tags = None; // clear tags
                    self.current_and_past();
                }
                Err(e) => self.msg_to_user = Some(format!("Cannot start activity: {}", e)),
            },
            Msg::Stop => match Activity::stop(None) {
                Ok(_) => {
                    self.msg_to_user = Some("Activity Stopped".to_string());
                    self.current_and_past();
                }
                Err(e) => self.msg_to_user = Some(format!("Cannot stop activity: {}", e)),
            },
            Msg::EditActivity(activity) => println!("{:?}", activity),
            Msg::DeleteActivity(index) => match Activity::delete(index) {
                Ok(_) => {
                    self.msg_to_user = Some("Activity Deleted".to_string());
                    self.current_and_past();
                }
                Err(e) => self.msg_to_user = Some(format!("Error deleting activity: {}", e)),
            },
            Msg::TagsInputChanged(new_value) => self.tags = Some(vec![new_value]),
        }
        // put focus on the tag text_input (if it doesn't exist, nothing happens)
        text_input::focus::<Msg>(TAG_INPUT.clone())
    }
}
