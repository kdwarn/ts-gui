/// Custom theme.
use iced::theme::{palette, Palette};
use iced::widget::{button, container, scrollable, text, text_input};
use iced::{application, Color, Vector};

const DARK_PURPLE: Color = Color {
    r: 55.0 / 255.0,
    g: 40.0 / 255.0,
    b: 62.0 / 255.0,
    a: 1.0,
};

const TWO_THIRDS_PURPLE: Color = Color {
    r: 55.0 / 255.0,
    g: 40.0 / 255.0,
    b: 62.0 / 255.0,
    a: 0.67,
};

const ONE_THIRD_PURPLE: Color = Color {
    r: 55.0 / 255.0,
    g: 40.0 / 255.0,
    b: 62.0 / 255.0,
    a: 0.34,
};

const YELLOW_WHITE: Color = Color {
    r: 235.0 / 255.0,
    g: 219.0 / 255.0,
    b: 178.0 / 255.0,
    a: 1.0,
};

const DARK_GRAY: Color = Color {
    r: 70.0 / 255.0,
    g: 66.0 / 255.0,
    b: 66.0 / 255.0,
    a: 1.0,
};

const DARKEST_GRAY: Color = Color {
    r: 40.0 / 255.0,
    g: 40.0 / 255.0,
    b: 40.0 / 255.0,
    a: 1.0,
};

#[derive(Debug, Clone, Copy, Default)]
pub struct TimeStudyTheme;

impl application::StyleSheet for TimeStudyTheme {
    type Style = ();

    fn appearance(&self, _style: &Self::Style) -> application::Appearance {
        application::Appearance {
            background_color: DARKEST_GRAY,
            text_color: YELLOW_WHITE,
        }
    }
}

impl TimeStudyTheme {
    pub fn palette(&self) -> Palette {
        Palette::DARK
    }

    pub fn extended_palette(&self) -> &palette::Extended {
        &palette::EXTENDED_DARK
    }
}

impl text::StyleSheet for TimeStudyTheme {
    type Style = ();

    fn appearance(&self, _style: Self::Style) -> text::Appearance {
        text::Appearance { color: None }
    }
}

impl text_input::StyleSheet for TimeStudyTheme {
    type Style = ();

    fn active(&self, _style: &Self::Style) -> text_input::Appearance {
        let palette = self.extended_palette();

        text_input::Appearance {
            background: palette.background.base.color.into(),
            border_radius: 2.0,
            border_width: 1.0,
            border_color: palette.background.strong.color,
        }
    }

    fn hovered(&self, _style: &Self::Style) -> text_input::Appearance {
        let palette = self.extended_palette();

        text_input::Appearance {
            background: palette.background.base.color.into(),
            border_radius: 2.0,
            border_width: 1.0,
            border_color: palette.background.base.text,
        }
    }

    fn focused(&self, _style: &Self::Style) -> text_input::Appearance {
        let palette = self.extended_palette();

        text_input::Appearance {
            background: palette.background.base.color.into(),
            border_radius: 2.0,
            border_width: 1.0,
            border_color: palette.primary.strong.color,
        }
    }

    fn placeholder_color(&self, _style: &Self::Style) -> Color {
        let palette = self.extended_palette();

        palette.background.strong.color
    }

    fn value_color(&self, _style: &Self::Style) -> Color {
        let palette = self.extended_palette();

        palette.background.base.text
    }

    fn selection_color(&self, _style: &Self::Style) -> Color {
        let palette = self.extended_palette();

        palette.primary.weak.color
    }
}

#[derive(Debug, Clone, Copy, Default)]
pub enum Container {
    #[default]
    Transparent,
    PastActivity,
}

impl container::StyleSheet for TimeStudyTheme {
    type Style = Container;

    fn appearance(&self, style: &Self::Style) -> container::Appearance {
        match style {
            Container::Transparent => container::Appearance {
                text_color: None,
                background: None,
                border_radius: 0.0,
                border_width: 0.0,
                border_color: Color::TRANSPARENT,
            },
            Container::PastActivity => container::Appearance {
                background: DARK_PURPLE.into(),
                border_color: Color::TRANSPARENT,
                border_width: 1.0,
                border_radius: 4.0,
                ..Default::default()
            },
        }
    }
}

#[derive(Debug, Clone, Copy, Default)]
pub enum Button {
    #[default]
    Primary,
    StartStop,
    PastActivity,
}

impl button::StyleSheet for TimeStudyTheme {
    type Style = Button;

    fn active(&self, style: &Self::Style) -> button::Appearance {
        match style {
            Button::Primary => button::Appearance {
                text_color: YELLOW_WHITE,
                background: DARK_GRAY.into(),
                border_radius: 4.0,
                border_width: 1.0,
                border_color: DARK_PURPLE,
                ..Default::default()
            },
            Button::PastActivity => button::Appearance {
                text_color: YELLOW_WHITE,
                background: DARK_GRAY.into(),
                border_radius: 4.0,
                border_width: 1.0,
                border_color: DARKEST_GRAY,
                ..Default::default()
            },
            Button::StartStop => button::Appearance {
                text_color: YELLOW_WHITE,
                background: DARK_PURPLE.into(),
                border_radius: 200.0,
                border_width: 2.0,
                border_color: DARK_GRAY,
                shadow_offset: Vector { x: 10.0, y: 10.0 },
            },
        }
    }
    fn hovered(&self, style: &Self::Style) -> button::Appearance {
        match style {
            Button::Primary => button::Appearance {
                ..self.active(style)
            },
            Button::PastActivity => button::Appearance {
                ..self.active(style)
            },
            Button::StartStop => button::Appearance {
                background: TWO_THIRDS_PURPLE.into(),
                shadow_offset: Vector { x: 5.0, y: 5.0 },
                ..self.active(style)
            },
        }
    }
    fn pressed(&self, style: &Self::Style) -> button::Appearance {
        match style {
            Button::Primary => button::Appearance {
                ..self.active(style)
            },
            Button::PastActivity => button::Appearance {
                ..self.active(style)
            },
            Button::StartStop => button::Appearance {
                background: ONE_THIRD_PURPLE.into(),
                shadow_offset: Vector { x: 0.0, y: 0.0 },
                ..self.active(style)
            },
        }
    }
}

impl scrollable::StyleSheet for TimeStudyTheme {
    type Style = ();

    fn active(&self, _style: &Self::Style) -> scrollable::Scrollbar {
        let palette = self.extended_palette();

        scrollable::Scrollbar {
            background: palette.background.weak.color.into(),
            border_radius: 2.0,
            border_width: 0.0,
            border_color: Color::TRANSPARENT,
            scroller: scrollable::Scroller {
                color: palette.background.strong.color,
                border_radius: 2.0,
                border_width: 0.0,
                border_color: Color::TRANSPARENT,
            },
        }
    }

    fn hovered(&self, _style: &Self::Style) -> scrollable::Scrollbar {
        let palette = self.extended_palette();

        scrollable::Scrollbar {
            background: palette.background.weak.color.into(),
            border_radius: 2.0,
            border_width: 0.0,
            border_color: Color::TRANSPARENT,
            scroller: scrollable::Scroller {
                color: palette.primary.strong.color,
                border_radius: 2.0,
                border_width: 0.0,
                border_color: Color::TRANSPARENT,
            },
        }
    }

    fn dragging(&self, style: &Self::Style) -> scrollable::Scrollbar {
        self.hovered(style)
    }
}
